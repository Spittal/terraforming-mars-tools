import { database } from '@/main';
import { startingResources, resourceKeys } from '@/shared/player.helper';
import { HashGenerator } from '@/shared/HashGenerator';
import router from '@/core/app-routes';

export const init = ({ commit, state, dispatch }) => {
  let gameId = state.gameId;

  if (router.app._route.params.gameId) {
    gameId = router.app._route.params.gameId;
  }

  commit('setGameId', { gameId });

  if (state.gameId) {
    dispatch('retrieveGameState');
  }
};

export const retrieveGameState = ({ state, dispatch, commit }) => {
  database.ref('/game/' + state.gameId).once('value').then((snapshot) => {
    const retrievedGameState = snapshot.val();

    if (retrievedGameState) {
      commit('changeGameState', { newState: retrievedGameState });

      if (!state.game[state.playerId]) {
        dispatch('addMyselfToGame');
      }

      dispatch('listenForChanges');
    }
  });
};

export const createNewGame = ({ state, dispatch, commit }) => {
  commit('setGameId', { gameId: HashGenerator.generateHash() });
  database.ref('/game/' + state.gameId).set({
    status: 'in-progress'
  });
  dispatch('addMyselfToGame');
  dispatch('listenForChanges');
};

export const addMyselfToGame = ({ state, dispatch }) => {
  database.ref('/game/' + state.gameId + '/' + state.playerId).set(startingResources);
  if (state.playerName) {
    dispatch('setPlayerName', { playerName: state.playerName });
  }
};

export const setPlayerName = ({ state, commit }, { playerName }) => {
  commit('setPlayerName', { playerName });
  database.ref('/game/' + state.gameId + '/' + state.playerId + '/playerName').set(playerName);
};

export const listenForChanges = ({ commit, state }) => {
  database.ref('/game/' + state.gameId)
    .on('value', snapshot => {
      commit('changeGameState', { newState: snapshot.val() });
    }
  );
};

export const changePlayerValueZeroCheck = ({ commit, state }, { key, value }) => {
  const newValue = state.game[state.playerId][key] + value;
  if (newValue >= 0) {
    database.ref('/game/' + state.gameId + '/' + state.playerId + '/' + key).set(newValue);
  }
};

export const changePlayerValue = ({ commit, state }, { key, value }) => {
  const newValue = state.game[state.playerId][key] + value;
  database.ref('/game/' + state.gameId + '/' + state.playerId + '/' + key).set(newValue);
};

export const produce = ({ state }) => {
  let afterProduce = Object.assign({}, state.game[state.playerId]);

  // Move energy over
  afterProduce.heat += afterProduce.energy;
  afterProduce.energy = 0;

  // Produce
  resourceKeys.map(resource => {
    afterProduce[resource] += afterProduce[resource + 'P'];
  });

  database.ref('/game/' + state.gameId + '/' + state.playerId).set(afterProduce);
};

export const completeGame = ({ state }) => {
  database.ref('/game/' + state.gameId + '/status').set('completed');
};
